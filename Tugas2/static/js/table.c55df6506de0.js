$(document).ready(function() {
	var url = window.location.href;

	var home = "home";
	var check = url.indexOf(home);
	if (check >= 0) {
		var d = document.getElementById("navbar-home");
		d.className += "active";
	}
	
	var artist = "artist";
	check = url.indexOf(artist);
	if (check >= 0) {
		var d = document.getElementById("navbar-artist");
		d.className += "active";
	}

	var aboutus = "about-us";
	check = url.indexOf(aboutus);
	if (check >= 0) {
		var d = document.getElementById("navbar-aboutus");
		d.className += "active";
	}
	
	var contactus = "contact-us";
	check = url.indexOf(contactus);
	if (check >= 0) {
		var d = document.getElementById("navbar-contactus");
		d.className += "active";
	}
});