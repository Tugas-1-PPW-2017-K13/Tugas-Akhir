from django.conf.urls import url
from .views import index,change_nilai

urlpatterns = [
    url(r'^$', index, name='index'),
	url(r'^change_nilai', change_nilai, name='change_nilai'),
]
