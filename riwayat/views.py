from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .helper.helper import RiwayatHelper
from .models import show_nilai
#from login.views import 
# Create your views here.
response = {}
helper = RiwayatHelper()

def index(request):
	riwayat_list = helper.instance.get_riwayat_list()
	response['riwayat_list']=riwayat_list
	response['show_nilai']=show_nilai.objects.last()
	html = 'riwayat/riwayat.html'
	return render(request, html, response)

def change_nilai(request):
	if(request.method == 'POST'):
		if(request.POST['show_nilai'] == 'True'):
			response['show_nilai']=True
		elif(request.POST['show_nilai'] == 'False'):
			response['show_nilai']=False
		Show_nilai = show_nilai(reminder=response['show_nilai'])
		Show_nilai.save()
		return HttpResponseRedirect('/riwayat/')
	else:
		return HttpResponseRedirect('/riwayat/')
