from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    status_attrs = {
        'type': 'text',
        'class': 'container status-form-textarea textarea',
        'cols': '90',
        'rows': '5',
        'placeholder':'Apa yang sedang kamu pikirkan?'
    }

    status = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=status_attrs))

show_form = forms.BooleanField(widget=forms.CheckboxInput(attrs={'class':'onoffswitch-checkbox','id': 'myonoffswitch'}))