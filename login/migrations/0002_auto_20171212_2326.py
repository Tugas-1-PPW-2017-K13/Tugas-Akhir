# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-12 23:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('login', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='status',
            field=models.TextField(blank=True, null=True),
        ),
    ]
