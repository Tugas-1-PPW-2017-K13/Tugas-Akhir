from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Pengguna(models.Model):
	kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True, )
	nama = models.CharField('Nama', max_length=200)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

# Create your models here.
class Status(models.Model):
	status = models.TextField(null=True, blank=True)
	created_date = models.DateTimeField(auto_now_add=True)
	# pengguna = models.ForeignKey(Pengguna, on_delete=models.CASCADE)

	class Meta:
		ordering = ('status', 'created_date')

class show_nilai(models.Model):
	reminder = models.BooleanField(default=True)