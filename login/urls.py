from django.conf.urls import url
from .views import index, dashboard, add_status, delete_status, change_nilai
from .custom_auth import auth_login, auth_logout

urlpatterns = [
	# custom auth
	url(r'^custom_auth/login/$', auth_login, name='auth_login'),
	url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),

	# index dan dashboard
	url(r'^$', index, name='index'),
	# url(r'^dashboard/$', dashboard, name='dashboard'),
	url(r'^(?P<username>.*)/$', dashboard, name='dashboard'),

	# status
	url(r'^add_status', add_status, name='add_status'),
	url(r'^dashboard/delete/(?P<pk>\d+)/$', delete_status, name='delete_status'),
	
	# Show Nilai
	url(r'^change_nilai', change_nilai, name='change_nilai'),
]
