from django.test import TestCase, Client
from django.urls import resolve
from .views import show_nilai,change_nilai
from django.http import HttpRequest
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.
class LoginTest(TestCase):
	def test_login_url_is_exist(self):
		response = Client().get('/login/')
		self.assertEqual(response.status_code, 200)

	def setUp(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")

	def test_login_url_is_exist(self):
		response = Client().get('/login/')
		self.assertEqual(response.status_code, 200)

	def test_login_failed(self):
		response = self.client.post('/login/custom_auth/login/', {'username': "siapa", 'password': "saya"})
		html_response = self.client.get('/login/').content.decode('utf-8')
		self.assertIn("Username atau password salah", html_response)

	def test_login_page_when_user_is_logged_in_or_not(self):
		#not logged in, render login template
		response = self.client.get('/login/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed('login/login.html')

		#logged in, redirect to profile page
		response = self.client.post('/login/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/login/')
		self.assertEqual(response.status_code, 302)
		self.assertTemplateUsed('login/dashboard.html')

	def test_direct_access_to_profile_url(self):
		#not logged in, redirect to login page
		response = self.client.get('/login/dashboard/')
		self.assertEqual(response.status_code, 302)

		#logged in, render profile template
		response = self.client.post('/login/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/login/dashboard/')
		self.assertEqual(response.status_code, 200)

	def test_logout(self):
		response = self.client.post('/login/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.post('/login/custom_auth/logout/')
		html_response = self.client.get('/login/').content.decode('utf-8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Anda berhasil logout. Semua session Anda sudah dihapus", html_response)
	

class RiwayatTest(TestCase):
	def setUp(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		
	def test_change_list_url_is_exist(self):
		response = self.client.post('/login/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.post('/login/change_nilai/'+'True/')
		html_response = self.client.get('/login/dashboard/').content.decode('utf8')
		self.assertEqual(response.status_code, 200)
		self.assertIn("This is nilai reminder", html_response)

