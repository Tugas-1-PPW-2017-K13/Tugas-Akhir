# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from .forms import Status_Form, show_form
from .models import Status, show_nilai, Pengguna
from .helper.helper import RiwayatHelper
from .utils import *

# Create your views here.
response = {}
helper = RiwayatHelper()

### USER
def index(request):
	# print ("#==> masuk index")
	if 'user_login' in request.session:
		username = request.session['user_login']
		response['login'] = True
		return HttpResponseRedirect('/login/' + username)
	else:
		response['author'] = get_data_user(request, 'user_login')
		html = 'login/login.html'
		return render(request, html, response)

def dashboard(request,username):
	print ("#==> dashboard")
	if not 'user_login' in request.session.keys():
		return HttpResponseRedirect(reverse('login:index'))
	else:
		set_data_for_session(request)
		kode_identitas = get_data_user(request, 'kode_identitas')
		try:
			pengguna = Pengguna.objects.get(kode_identitas = kode_identitas)
		except Exception as e:
			pengguna = create_new_user(request)
		response['feed']=len(Status.objects.all().order_by('-created_date'))
		status = Status.objects.all()
		response['database'] = status
		response['status_form'] = Status_Form
		riwayat_list = helper.instance.get_riwayat_list()
		response['riwayat_list']=riwayat_list
		response['show_nilai']=show_nilai.objects.last()
		html = 'login/dashboard.html'
		return render(request, html, response)

### SESSION : GET and SET
def get_data_session(request):
	if get_data_user(request, 'user_login'):
		response['author'] = get_data_user(request, 'user_login')

def set_data_for_session(request):
	response['author'] = get_data_user(request, 'user_login')
	response['kode_identitas'] = request.session['kode_identitas']
	response['role'] = request.session['role']

### STATUS
def add_status(request):
	form = Status_Form(request.POST)
	if(request.method == 'POST' and form.is_valid()):
		response['status'] = request.POST['status']
		status = Status(status=response['status'])
		status.save()
		return HttpResponseRedirect('/login/dashboard/')
	else:
		return HttpResponseRedirect('/login/dashboard/')


def delete_status(request, pk):
	status = Status.objects.filter(pk=pk).first()
	if status != None:
		status.delete()
		pass
	return HttpResponseRedirect('/login/dashboard/')

def change_nilai(request):
	if(request.method == 'POST'):
		if(request.POST['show_nilai'] == 'True'):
			response['show_nilai']=True
		elif(request.POST['show_nilai'] == 'False'):
			response['show_nilai']=False
		Show_nilai = show_nilai(reminder=response['show_nilai'])
		Show_nilai.save()
		return HttpResponseRedirect('/login/dashboard/')
	else:
		return HttpResponseRedirect('/login/dashboard/')

