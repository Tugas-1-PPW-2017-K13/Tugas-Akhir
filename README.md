# Tugas 2 PPW

## Pipelines status
[![pipeline status](https://gitlab.com/Tugas-1-PPW-2017-K13/Tugas-Akhir/badges/master/pipeline.svg)](https://gitlab.com/Tugas-1-PPW-2017-K13/Tugas-Akhir/commits/master)

## code coverage status
[![coverage report](https://gitlab.com/Tugas-1-PPW-2017-K13/Tugas-Akhir/badges/master/coverage.svg)](https://gitlab.com/Tugas-1-PPW-2017-K13/Tugas-Akhir/commits/master)

## Team composition:
1. Elvan Rizky Novandi |NPM: 1606822756
2. Michael Tengganus   |NPM: 1606917651
3. Muhammad Aulia Adil |NPM: 1606874671

## heroku link
	http://ppwlinkedin.herokuapp.com/
